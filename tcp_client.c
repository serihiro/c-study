#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#define BUFFERSIZE 256

int
main(int argc, char *argv[]) {
    if(argc < 3) {
        puts("Usage: /path/to/binary host port");
        exit(1);
    }

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0) {
        perror("Failed to create socket\n");
        exit(1);
    }

    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));

    struct hostent *host = gethostbyname(argv[1]);
    if(host == NULL) {
        perror("Faild to gethostbyname");
        exit(1);
    }
    server_address.sin_family = AF_INET;
    server_address.sin_port   = htons(atoi(argv[2]));
    server_address.sin_addr.s_addr = *(unsigned int *)host->h_addr_list[0];

    int connect_result = connect(socket_fd, (struct sockaddr *)&server_address, sizeof(server_address));
    if ( connect_result < 0) {
        perror("Failed to connect server");
        exit(1);
    }
    char buf[BUFFERSIZE];
    memset(buf, 0, sizeof(buf));
    int send_result, rcv_result;
    while(1) {
        puts("waiting for input...");
        scanf("%s", buf);
        send_result = send(socket_fd, buf, sizeof(buf), 0);
        if (send_result < 0) {
            perror("Failed to send");
            exit(1);
        }

        rcv_result = recv(socket_fd, buf, sizeof(buf), 0);
        if (rcv_result < 0 ) {
            perror("Failed to recv");
            exit(1);
        }
        if (!rcv_result) {
            perror("Server has gone");
            exit(1);
        }

        printf("%s\n", buf);
    }

    close(socket_fd);
    return(0);
}
