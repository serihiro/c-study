#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int
main(){
    char c;
    int src, dst; //file discripter, not a pointer
    int count;

    src = open("src.txt", O_RDONLY);
    if (src < 0) {
        perror("error open src.txt");
        exit(1);
    }

    dst = open("dst.txt", O_WRONLY | O_CREAT, O_TRUNC, 0666);
    if (dst < 0) {
        perror("error open dst.txt");
        close(src);
        exit(1);
    }

    while ((count = read(src, &c, 1)) > 0) {
        printf("%d ", count);
        putchar('\n');
        putchar(c);
        putchar('\n');

        if (write(dst, &c, count) < 0) {
                perror("failed to write");
                close(src);
                close(dst);
                exit(1);
        }
    }

    if (count < 0){
        perror("could not read");
        close(src);
        close(dst);
        exit(1);
    }

    close(src);
    close(dst);

    return 0;
}
