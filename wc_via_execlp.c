#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {
    printf("start");

    if(argc != 2) {
        printf("Usage: %s file_name\n", argv[0]);
        exit(1);
    }

    // // 標準入力でパスを受け取ってopenしたテキストファイルのファイルディスクリプタ
    int file_fd;
    file_fd = open(argv[1], O_RDONLY);
    if(file_fd < 0) {
        perror("open(argv[1], O_RDONLY);");
        close(file_fd);
        exit(1);
    }
    //
    // // 標準入力を閉じる
    // // STDIN_FILENO == 0
    // // see also https://www.gnu.org/software/libc/manual/html_node/Descriptors-and-Streams.html
    close(STDIN_FILENO);
    //
    // // openしたテキストファイルのファイルディスクリプタをSTDIN_FILENOに付け替える
    if(dup2(file_fd, STDIN_FILENO ) < 0 ){
        perror("dup2(file_fd");
        close(file_fd);
        exit(1);
    }
    close(file_fd);

    // 第二引数は可変長引数であるが、最初に実行するコマンドを書いておく
    // そうしなくてはいけない理由として、第一引数が実行可能なコマンドではない場合の
    // 保険として、第二引数に実行するコマンドを入れとくと探索順の関係で
    // 保険になるので入れておく、ということらしい。
    // 実際この場合はwcコマンドはちゃんとちゃんと探索できるので
    // 第二引数にaobaとかooiとか入れてもちゃんと動く
    // NULLで終わるようにしているのはそういう約束なので
    // see also https://linuxjm.osdn.jp/html/LDP_man-pages/man3/exec.3.html
    execlp("/usr/bin/wc", "wc", NULL);
    //        4       6      14
    //
    // e.g. -lオプションを入れたい場合は
    // execlp("/usr/bin/wc", "wc", "-l", NULL);
    return(0);
}
