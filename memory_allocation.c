#include <stdio.h>

extern char **environ;

int     data0;
int     data1 = 10;

int
main(int argc, char *argv[]){
    char    c;

    //%p prints the address of the pointer.
    printf("environ:\t\t%p\n", environ);
    printf("argv:\t\t%p\n", argv);
    printf("stack:\t\t%p\n", &c);

    printf("bss:\t\t%p\n", &data0);
    printf("data:\t\t%p\n", &data1);

// see more detail: http://www.coins.tsukuba.ac.jp/~syspro/2016/shui/1stHalf.html#sec:process-concepts
// environ:        0x7fff5ec29c30
// argv:           0x7fff5ec29c20
// stack:          0x7fff5ec29bef
// bss:            0x100fd6024
// data:           0x100fd6020

    return 0;
}
