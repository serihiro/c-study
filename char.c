#include <stdio.h>

// This is same as char s[] = "Hello";
char s[] = {'H','e','l','l','o',0};

char *ps = "Hello";

int
main(){
    char c = 'a';
    while (c <= 'z'){
        putchar(c++);
    }
    putchar('\n');

    printf("%s\n", s);
    int i = 0;
    while(s[i]){
        printf("[%d] = %c\n", i, s[i]);
        i++;
    }

    // This occurs a warning: warning: overflow in implicit constant conversion [-Woverflow]
    // Maybe gcc does a specific process for char pointer definition.
    // *ps = 'Hello';
    i = 0;
    printf("%s\n", ps);
    while (*ps) {
        printf("[%d] = %c\n", i, *ps);
        i++;
        ps++;
    }

    return 0;
}
