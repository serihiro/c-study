#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFFERSIZE  1024

int
main(int argc, char *argv[]) {
    if(argc < 2) {
        puts("Usage: /path/to/binary port");
        exit(1);
    }

    /*
        接続要求を受け付けるsocketを作成する。
        この関数で生成されるファイルディスクリプタを経由してデータをやり取りする訳ではなく、
        多分クライアントとSYN-SYNACK-SYNを送り合う関係。

        第一引数は通信ドメインを指定する。PF_INETはip v4
        PFはProtocol Familyの略で、socketに渡すものとして定義されている。
        AFはAddresss Familyの略で、connect関数に渡すものとして定義されている。
        AFの方のipv4はAF_INETとして定義されているが、中身は同じらしい。

        第二引数は通信形式を指定する。
        SOCK_STREAM 順次双方向バイトストリーム。コネクション型。信頼性が高い。
        SOCK_DGRAM  データグラム。コネクションレス型。信頼性が低い。UDPで提供。
        SOCK_ROW    直接IPを用いた通信を行なう

        第三引数はprotocolを指定する。
        0           自動設定(AF_INET&SOCK_RAWでIPを直接扱いたい場合も含む)
        IPPROTO_TCP TCP/IP(AF_INET&SOCK_STREAMの場合。0も可。)
        IPPROTO_UDP UDP/IP(AF_INET&SOCK_DGRAMの場合。0も可。)
        IPPROTO_RAW ICMP(pingコマンドなど。AF_INET&SOCK_RAWでICMPソケットを作りたい場合)
    */
    int request_socket_fd = socket(PF_INET, SOCK_STREAM, 0);
    if(request_socket_fd < 0) {
        perror("Failed to create socket\n");
        exit(1);
    }

    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = PF_INET;
    server_address.sin_port   = htons(atoi(argv[1]));

    printf("server_address.sin_len: %d\n", server_address.sin_len);
    printf("server_address.sin_family: %d\n", server_address.sin_family);
    printf("server_address.sin_port: %d\n", server_address.sin_port);
    printf("server_address.sin_zero: %s\n", server_address.sin_zero);
    printf("server_address.sin_addr.s_addr: %d\n", server_address.sin_addr.s_addr);

    /*
        manコマンド見ても「bind a name to a socket」とか書いてあってなんのこっちゃいという感じがする。
        「bind a name」っていうのは要するにsocketにアドレスを紐付けるということらしい。
    */
    int bind_result = bind(request_socket_fd, (struct sockaddr *)&server_address, sizeof(struct sockaddr_in));
    if(bind_result < 0) {
        perror("Failed to bind\n");
        exit(1);
    }

    /*
        socket上の接続要求を待つ
        第一引数sockfd SOCK_STREAM 型か SOCK_SEQPACKET 型のソケットを参照するファイルディスクリプターである。
        第二引数backlog sockfd についての保留中の接続のキューの最大長を指定する。
                      キューがいっぱいの状態で接続要求が到着すると、クライアントは ECONNREFUSED
                      というエラーを受け取る。
                      下位層のプロトコルが再送信をサポート していれば、要求は無視され、
                      これ以降の接続要求の再送信が成功するかもしれない。
    */
    int listen_result = listen(request_socket_fd, 5);
    if(listen_result < 0) {
        perror("Failed to listen\n");
        exit(1);
    }

    struct sockaddr_in client_address;
    memset(&client_address, 0, sizeof(client_address));

    puts("***before accept***");
    printf("client_address.sin_len: %d\n", client_address.sin_len);
    printf("client_address.sin_family: %d\n", client_address.sin_family);
    printf("client_address.sin_port: %d\n", client_address.sin_port);
    printf("client_address.sin_zero: %s\n", client_address.sin_zero);
    printf("client_address.sin_addr.s_addr: %d\n", client_address.sin_addr.s_addr);

    /*
        clientからのsocketへのリクエストを受け付ける。
        第一引数にはlistenしてるsocketのfdを渡す。
        JF projectのmanコメントの日本語訳には
        「引き数 sockfd は、 socket(2) によって生成され、 bind(2) によってローカルアドレスにバインドされ、
         listen(2) を経て接続を待っているソケットである」
         と書いてあり、非常にリズム感のある文体となっている。

        第二引数にはには接続先のclientのアドレスを格納するsockaddr構造体のポインタを渡す

        第三引数は入出力両用の引き数である。
        呼び出し時には、呼び出し元が addr が指す構造体のサイズ (バイト単位) で初期化しておかなければならない。 返ってくる時には、接続相手のアドレスの実際の大きさが格納される。
    */
    int accept_socket_fd;
    socklen_t len = sizeof(client_address);
    accept_socket_fd = accept(request_socket_fd, (struct sockaddr*)&client_address, &len);
    if (accept_socket_fd < 0){
        perror("Failed to accept\n");
        exit(1);
    }

    puts("***after accept***");
    printf("client_address.sin_len: %d\n", client_address.sin_len);
    printf("client_address.sin_family: %d\n", client_address.sin_family);
    printf("client_address.sin_port: %d\n", client_address.sin_port);
    printf("client_address.sin_zero: %s\n", client_address.sin_zero);
    printf("client_address.sin_addr.s_addr: %d\n", client_address.sin_addr.s_addr);

    char buf[BUFFERSIZE];
    while(1) {
        int recv_message_size = recv(accept_socket_fd, buf, BUFFERSIZE, 0);
        if (!recv_message_size) {
            puts("Client has gone");
            break;
        }
        if (recv_message_size < 0) {
            perror("Failed to recv");
            exit(1);
        }
        printf("recv message: %s\n", buf);
        printf("recv_message_size: %d\n", recv_message_size);
        int send_message_size = send(accept_socket_fd, buf, recv_message_size, 0);
        if(send_message_size < 0) {
            perror("Failed to send");
            exit(1);
        }
        printf("write_message_size: %d\n", send_message_size);
    }

    close(accept_socket_fd);
    close(request_socket_fd);

    puts("Bye bye...");
    return (0);
}
