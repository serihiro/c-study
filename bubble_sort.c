#include <stdio.h>

void
swap_array(int array[], int i, int j) {
    int work;

    work = array[i];
    array[i] = array[j];
    array[j] = work;
}

void
sort(int data[], int count)
{
  int i,j;
  int n = count-1;

  for(i=0; i<n; i++){
    for(j=0; j<(n-i); j++){
      int k=0;
      for(k=0;k<count;k++)
        printf("%2d ", data[k]);
      printf("\n");
      if(data[j] > data[j+1]){
        swap_array(data, j, j+1);
      }
    }
  }
}

void
print_data(int array[], int count) {
  int i;
  for(i=0; i<count; i++){
    printf("%2d \n", array[i]);
  }
}

#define SAMLE_COUNT 6
int sample[SAMLE_COUNT] = {8, 12, 3, 15, 7, 4};

void
main() {
  sort(sample, SAMLE_COUNT);
  print_data(sample, SAMLE_COUNT);
}
