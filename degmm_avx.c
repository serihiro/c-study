// to compile this file, add option -O3 -mavx2
// e.g.
// $ clang ./degmm_x86_avx.c -O3 -mavx2

#include "immintrin.h"
#include "stdio.h"

void dgemm(int n, double* A, double* B, double* C){
    for(int i=0; i<n; i+=4){
        for(int j=0; j<n; j++){
            __m256d c0 = _mm256_load_pd(C + i + j * n); /* c0 = C[i][j] */

            for(int k=0; k<n; k++){
                /* c0 += A[i][k] * B[k][j] */
                c0 = _mm256_add_pd(
                    c0,
                    _mm256_mul_pd(
                        _mm256_load_pd(A + i + k * n),
                        _mm256_broadcast_sd(B + k * j * n)
                    )
                );

                _mm256_store_pd(C + i + j * n, c0); /*C[i][j] = c0 */
            }
        }
    }
}

// to avoid undefined reference to `WinMain@16' in Windows
int main(int argc, char ** args){
    double A[4] = {1.0, 3.0, 2.0, 4.0};
    double B[4] = {5.0, 7.0, 6.0, 8.0};
    double C[4] = {0.0, 0.0, 0.0, 0.0};

    dgemm(4, A, B, C);

    printf("%f", C[0]);
    printf("%f", C[2]);
    printf("%f", C[1]);
    printf("%f", C[3]);

    return 0;
}
