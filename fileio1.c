#include <stdio.h>
#include <stdlib.h>

int
main()
{
    int c;
    FILE *src, *dst;

    src = fopen("src.txt", "r");
    if (src == NULL) {
        perror("failed to fopen src.txt");
        exit(1);
    }

    dst = fopen("dst.txt","w");
    if (dst == NULL) {
        perror("failed to fopen dst.txt");
        exit(1);
    }
    while((c = fgetc(src)) != EOF){
        fputc(c, dst);
    }

    fclose(src);
    fclose(dst);

    return 0;
}
