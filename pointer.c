#include <stdio.h>

int
main() {
    int i = 5;
    int j = 4;

    printf("%2d ", &i);
    printf("%2d ", &j);

    int *p = &i;
    int *q = &j;

    printf("%2d ", *p);
    printf("%2d ", *q);

    *p = 10;
    printf("%2d ", *p);
    printf("%2d ", i);

    return 0;
}
