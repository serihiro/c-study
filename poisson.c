#define  CRT SECURE NO WARNINGS // これは、無くてすむ環境なら削除可
#include <stdio.h>
#include <math.h>

#define   NPOIS     10000
#define   URAND(r)  (r=(a*r+c)%m)/(double)m

int main(void){
  FILE    *fp;        // 出力ファイル
  int     a,c,m;      // 乱数系列の係数
  int     r;          // [0,INT MAX) 乱数
  double  z;          // [0,1) 乱数
  int     k;          // ポアソン乱数発生カウンタ
  int     p;          // ポアソン乱数：NPOIS個
  int     pmx=0;      // ポアソン乱数発生最大値
  int     h[20]={0};  // ポアソン分布
  double  lmd;        // λ：e^λ
  double  y;          // (e^λ)*Πz：>1 の間、p++

  fp= fopen("ポアソン乱数.cvl","w");
  a = 1229;
  c = 351750;
  m = 1664501;

  printf("λ=1.76\n");
  lmd = 1.76;
  r = 1;
  for(k=0;k<NPOIS;k++){
    y = exp(lmd);
    for(p=0;y>1;p++){
      z = URAND(r);
      y = y*z;
   //@printf("k=%d: p=%d z=%f y=%f: ",k,p,z,y);
   //@printf("counting p(poisson)\n");
    }
  //@printf(   "%d\n",p);
    fprintf(fp,"%d\n",p);
    h[p]++;
    if(pmx<p) pmx = p;
  }
  printf("ポアソン乱数分布：\n");
  for(p=0;p<pmx;p++){
    printf("%2d: %d\n",p,h[p]);
  }

  printf("\n-normal end-\n");
  fclose(fp);
  return 0;
}
