#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

volatile sig_atomic_t sigint_count = 3;

void
sigint_handler(int signum) {
    printf("sigint_handler(%d): sigint_count(%d)\n",
            signum, sigint_count);
    if(--sigint_count <= 0){
        printf("sigint_handler: exiting... \n");
        exit(1);
    }

/*
あるシグナルに対応したシグナルハンドラを実行中に，また同じシグナルが通知された場合，同じシグナルハンドラを起動してしまうと，データの一貫性などに問題が生じてしまう
そのため，シグナルハンドラは一度呼び出されるとデフォルトの動作にリセットされるようになっていた
そのため，シグナルハンドラが起動される度に signal システムコールでシグナルハンドラを設定し直す必要があった

マジかよ
*/
# if 0
    signal(SIGINT, &sigint_handler);
# endif
}

int
main() {
    signal(SIGINT, &sigint_handler);

    for(;;){
        printf("main: sigint_count(%d), calling pause... \n", sigint_count);
        pause();
        printf("main: return from pause. sigint_count(%d)\n", sigint_count);
    }
}
