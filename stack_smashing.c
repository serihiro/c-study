#include <stdio.h>
#include <string.h>

char  *longstr = "01234567890abcdefghijklmnopqrstuvwxyz"
          "01234567890abcdefghijklmnopqrstuvwxyz"
          "01234567890abcdefghijklmnopqrstuvwxyz"
          "01234567890abcdefghijklmnopqrstuvwxyz"
          "01234567890abcdefghijklmnopqrstuvwxyz"
          "01234567890abcdefghijklmnopqrstuvwxyz";

void
with_check(char *src)
{
    char    buf[20];
    int i;

    i = strlcpy(buf, src, sizeof(buf));

    if(i > sizeof(buf) - 1)
        printf("with_check: buf(%lu) is too short to copy src(%s)" ,sizeof(buf), src);

    printf("with_check: buf(%s)¥n", buf);
}

void
without_check(char *src) {
    char buf[20];
    strcpy(buf, src);
    printf("without_check: buf(%s)¥n", buf);
}

int
main(){
    with_check(longstr);
    //this causes segmentation fault
    without_check(longstr);

    return 0;
}
