#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

volatile sig_atomic_t alrm_count = 10;

void
alrm(){
    alrm_count--;
}

int
main(){
    struct sigaction sa_alarm;
    struct itimerval itimer;

    memset(&sa_alarm, 0, sizeof(sa_alarm));
    sa_alarm.sa_handler = alrm;
    sa_alarm.sa_flags = SA_RESTART;
    //SIGALRMを受け取った時のactionを定義
    if (sigaction(SIGALRM, &sa_alarm, NULL) < 0){
        perror("sigaction");
        exit(1);
    }

    itimer.it_value.tv_sec = itimer.it_interval.tv_sec = 1;
    itimer.it_value.tv_usec = itimer.it_interval.tv_usec = 0;
    // timerをスタートして一定間隔でSIGVTALRMを自分のprocessにkillする
    // forkした子プロセスで実行すると親プロセスにもkillできるようだが要検証。
    if(setitimer(ITIMER_REAL, &itimer, NULL) < 0) {
        perror("settimer");
        exit(1);
    }

    while(alrm_count){
        pause();
        printf("%d: %d\n", alrm_count, (int)time(NULL));
    }

    // timerを停止（しなくてもこの場合は問題なさげ）
    itimer.it_value.tv_sec  = itimer.it_interval.tv_sec  = 0;
    itimer.it_value.tv_usec = itimer.it_interval.tv_usec = 0;
    if (setitimer(ITIMER_REAL, &itimer, NULL) < 0) {
        perror("settimer");
        exit(1);
    }
}
