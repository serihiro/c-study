//筑波大学院2016年8月試験の過去問より

#include <stdlib.h>
#include <stdio.h>

struct pq {
    int n;
    int a[ 1000 ];
};

struct pq * newq ()
{
    struct pq * q = malloc(sizeof( struct pq ));
    q->n = 0;
    q->a [0] = 99999 ;
    return q ;
}

void putq ( struct pq *q , int v )
{
    int i = ++( q -> n );
    while (q->a[i/2] <= v ) {
        q->a[i] = q->a[i/2];
        i = i / 2;
    }
    q->a [i] = v;
}

int getq ( struct pq *q )
{
    int i = 1, j , v , w ;
    if (q-> n == 0) {
        return (-1) ;
    }

    v = q->a[1];
    w = q->a[(q->n)--];
    while (i <= (q->n) / 2) {
        j = 2 * i ;

        if ( j < q->n && q->a[j] < q->a[j +1]) j++;

        if ( w >= q->a[j]) {
            break ;
        } else {
            q->a[i] = q->a[j];
            i = j ;
        }
    }
    q->a[i] = w;
    return v ;
}

int main(){
    struct pq *queue = newq();
    putq(queue, 1);
    putq(queue, 2);
    putq(queue, 3);
    putq(queue, 4);
    putq(queue, 5);
    for(int i=0; i < 10; i++){
        printf("%d", queue->a[i]);
        puts("");
    }
    puts("");
    printf("%d", getq(queue));
    puts("");
    printf("%d", getq(queue));
    puts("");
    printf("%d", getq(queue));
    puts("");
    printf("%d", getq(queue));
    puts("");
    printf("%d", getq(queue));
    puts("");
    printf("%d", getq(queue));
    puts("");
    printf("%d", getq(queue));
    puts("");

    // for(int i=0; i < 10; i++){
    //     printf("%d", queue->a[i]);
    //     puts("");
    // }

    return 0;
}
