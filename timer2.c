#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

volatile sig_atomic_t alrm_count = 10;

void
alrm(){
    alrm_count--;
}

void
child() {
    printf("SIGCHLD received\n");
}

int
main(){
    struct sigaction sa_alarm;
    // スタック上に取られるローカル変数の初期値は不定であるため
    // struct sigaction 型の変数を0クリアする
    memset(&sa_alarm, 0, sizeof(sa_alarm));
    sa_alarm.sa_handler = alrm;
    sa_alarm.sa_flags = SA_RESTART;
    //SIGALRMを受け取った時のactionを定義
    if (sigaction(SIGALRM, &sa_alarm, NULL) < 0){
        perror("sigaction");
        exit(1);
    }

    struct sigaction sa_child_finish;
    memset(&sa_child_finish, 0, sizeof(sa_child_finish));
    sa_child_finish.sa_handler = child;
    sa_child_finish.sa_flags = SA_RESTART;
    //SIGCHLDを受け取った時のactionを定義
    if (sigaction(SIGCHLD, &sa_child_finish, NULL) < 0){
        perror("sigaction");
        exit(1);
    }

    if(fork() == 0){
        struct itimerval itimer;
        itimer.it_value.tv_sec = itimer.it_interval.tv_sec = 1;
        itimer.it_value.tv_usec = itimer.it_interval.tv_usec = 0;
        if(setitimer(ITIMER_REAL, &itimer, NULL) < 0) {
            perror("settimer");
            exit(1);
        }

        while(alrm_count){
            pause();
            printf("%d: %d\n", alrm_count, (int)time(NULL));
        }
    }else{
        // 親側で子プロセスがsetitimerで発生させたシグナルを拾えないか試したが
        // なんか無理っぽかった。
        // pause()は1回だけ解除されるので子プロセス生成時に何らかのsignalを受け取ってはいる模様
        // SIGCHLDっぽい -> sigaction入れて試したらそうっぽい
        // while(alrm_count){
        //     pause();
        //     printf("%d: %d\n", alrm_count, (int)time(NULL));
        // }
        wait(NULL);
        printf("end");
    }
}
