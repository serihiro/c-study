#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char    *buf5;
    char    *buf20;
    char    *d = "01234567890";
    char    i;

    buf5 = malloc(5);
    if(buf5 == NULL) {
        perror("buf5 malloc failed");
        exit(1);
    }

    buf20 = malloc(20);
    if(buf20 == NULL){
        perror("buf20 malloc failed");
        exit(1);
    }

    strlcpy(buf5, d, 5);
    printf("%s", buf5);

    free(buf5);
    free(buf20);
    return 0;
}
