#include <stdio.h>
#include <string.h>

int
main(){
    char    buf5[5];
    char    buf20[20];
    char    *d = "01234567890";
    char    *s = "abcdefghijklmnopqrstuvwxyz";
    int     i;

    i = snprintf(buf5, sizeof(buf5), "%s", d);
    printf("cpy  5: src(%s) len(%d) dst(%s) len(%d)\n",d, i, buf5, strlen(buf5));

     i = snprintf(buf20, sizeof(buf20), "%s", d);
     printf("cpy 20: src(%s) len(%d) dst(%s) len(%d)\n",d, i, buf20, strlen(buf20));

    i = snprintf(buf20, sizeof(buf20), "%s%s", d, s);
    printf("cat 20: dst(%s) len(%d)\n", buf20, strlen(buf20));

    return 0;
}
